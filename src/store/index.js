import { createStore } from "vuex";

import lokasi from "./module/lokasi";
import sholat from "./module/sholat";

export default createStore({
    modules: {
        lokasi,
        sholat
    }
})