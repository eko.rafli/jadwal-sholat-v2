import Api from "@/api/Api";

const sholat = {
    namespaced: true,

    state: {
        data: {},
        lokasi: ""
    },

    mutations: {
        SET_SHOLAT(state, data) {
            state.data = data
        },

        SET_LOKASI(state, lokasi) {
          state.lokasi = lokasi
        }
    },

    actions: {
        async getSholat({ commit }, payload) {
            try {
              const response = await Api.get(`/sholat/jadwal/${payload.lokasi}/${payload.tanggal}`);
              commit('SET_SHOLAT', response.data.data.jadwal)
              commit('SET_LOKASI', response.data.data.lokasi)
            } catch (error) {
              console.error('Error fetching sholat data:', error);
            }
          }
    },

    getters: {

    }
}

export default sholat