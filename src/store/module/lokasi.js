import Api from "@/api/Api"
const lokasi = {
    namespaced: true,

    state: {
        lokasi: {},
        tanggal: {}
    },

    mutations: {
        SET_LOKASI(state, lokasi) {
            state.lokasi = lokasi
        },
        SET_TANGGAL(state, tanggal) {
            state.tanggal = tanggal
        }
    },

    actions: {
        getLokasi({ commit }, input){
            if(input) {
                Api.get(`/sholat/kota/cari/${input}`)
                .then(response => {
                    commit('SET_LOKASI', response.data.data)
                }).catch(error => {
                    console.log(error)
                })
            } else {
                Api.get('/sholat/kota/semua')
                .then(response => {
                    commit('SET_LOKASI', response.data.data)
                }).catch(error => {
                    console.log(error)
                })
            }

            Api.get('/cal/hijr/?adj=-1')
            .then(response => {
                commit('SET_TANGGAL', response.data.data.date)
            }).catch(error => {
                console.log(error)
            })
        },

        setTanggal({ commit }) {
            Api.get('/cal/hijr/?adj=-1')
            .then(response => {
                commit('SET_TANGGAL', response.data.data.date)
            }).catch(error => {
                console.log(error)
            })
        }
    },

    getters: {

    }
}

export default lokasi