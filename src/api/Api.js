import axios from "axios";

const Api = axios.create({
    baseURL: 'https://api.myquran.com/v2'
})

export default Api