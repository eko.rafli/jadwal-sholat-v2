import { createApp } from 'vue'
import moment from 'moment'
import App from './App.vue'

import './index.css'
import router from './router'
import store from './store'

createApp(App)
.use(moment)
.use(router)
.use(store)
.mount('#app')
