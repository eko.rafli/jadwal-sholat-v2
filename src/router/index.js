import { createRouter, createWebHistory } from "vue-router";

const routes = [
    {
        path: '/',
        name: 'home',
        component: () => import( /* webpackChunkName: "home"*/ '@/views/dashboard/Home.vue')
    },
    {
        path: '/sholat',
        name: 'sholat',
        component: () => import( /* webpackChunkName: "sholat" */ '@/views/dashboard/Sholat.vue')
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router