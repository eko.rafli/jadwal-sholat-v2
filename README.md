# Frontend Jadwal Sholat

Page sederhana yang menampilkan jadwal sholat harian. Jadwal didapat dari <a href="https://api.myquran.com"  target="_blank">API myQuran V2</a> yang bersumber dari situs Bimas Islam Kemenag dengan metode scraping

# Preview
<a href="https://jadwalsholatv2.netlify.app/">jadwalsholatv2.netlify.app</a>


# Stack

- VueJS 3
- TailwindCSS
- <a href="https://api.myquran.com"  target="_blank">API myQuran V2</a>

# Instalasi
> npm install
## Development
> npm run serve

## Production
> npm run build

The file explorer is accessible using the button in left corner of the navigation bar. You can create a new file by clicking the **New file** button in the file explorer. You can also create folders by clicking the **New folder** button.

# Thanks
<a href="https://banghasan.com">Bang Hasan</a>

# Contact
Contact me @ <a href="mailto:eko.rafli@gmail.com">eko.rafli@gmail.com</a>
Feel free to use it